/**
 * Copyright (c) 2016-2019 Nikita Koksharov
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jigsaw;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.NumericDate;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.redisson.api.map.event.EntryCreatedListener;
import org.redisson.api.map.event.EntryEvent;
import org.redisson.config.Config;

import java.io.IOException;

import static org.redisson.example.services.JwtGenerator.generateSecretKey;

public class TransactionRMap2 {


    public static void main(String[] args) throws IOException, JoseException {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.25.41.98:6379").setPassword("Comviva@123");
        RedissonClient redisson = Redisson.create(config);


        RMapCache<String, Object> map = redisson.getMapCache("jwtCache");

        System.out.println("readToken  :::: " + map.readAllMap());


        int createListener = map.addListener(new EntryCreatedListener<Integer, Integer>() {
            @Override
            public void onCreated(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                System.out.println("RMapCache  ::::  onCreated ::::" + event.getKey() + "  " + event.getValue());
            }
        });

    }

}
