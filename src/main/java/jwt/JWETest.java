package jwt;

import org.jose4j.jwe.JsonWebEncryption;
import org.jose4j.jwe.KeyManagementAlgorithmIdentifiers;
import org.jose4j.jwe.ContentEncryptionAlgorithmIdentifiers;
import org.jose4j.lang.ByteUtil;
import org.jose4j.lang.JoseException;
import org.jose4j.keys.AesKey;

import java.security.Key;

public class JWETest {
    public static void main(String[] args) throws JoseException {
        // Create a key
        Key key = new AesKey(ByteUtil.randomBytes(16));

        // Create a new Json Web Encryption
        JsonWebEncryption senderJwe = new JsonWebEncryption();
        senderJwe.setPlaintext("Hello, World!");
        senderJwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.RSA1_5);
        senderJwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        senderJwe.setKey(key);

        // Serialize to compact form
        String compactSerialization = senderJwe.getCompactSerialization();

        // Receiver side: Parse the JWE string back into a JWE object
        JsonWebEncryption receiverJwe = new JsonWebEncryption();
        receiverJwe.setKey(key);
        receiverJwe.setCompactSerialization(compactSerialization);

        // Ensure the plaintext of the JWE is what you expected
        System.out.println(receiverJwe.getPlaintextString());
    }
}
