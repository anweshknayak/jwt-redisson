package jwt;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;
import org.jose4j.jwt.NumericDate;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;

public class JwtTokenCreator {

    public static void main(String[] args) throws JoseException {
        // Generate an RSA key pair, which will be used for signing and verification of the JWT, wrapped in a JWK
        RsaJsonWebKey rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);

        // Give the JWK a Key ID (kid), which is just the polite thing to do
        rsaJsonWebKey.setKeyId("k1");

        // Create the Claims, which will be the content of the JWT
        JwtClaims claims = new JwtClaims();
   //     claims.setIssuer("Issuer");  // who creates the token and signs it
     //   claims.setAudience("Audience"); // to whom the token is intended to be sent
        claims.setExpirationTimeMinutesInTheFuture(10); // time when the token will expire (10 minutes from now)
        claims.setGeneratedJwtId(); // a unique identifier for the token
        claims.setIssuedAtToNow();  // when the token was issued/created (now)
     //   claims.setNotBeforeMinutesInThePast(2); // time before which the token is not yet valid (2 minutes ago)
    //    claims.setSubject("subject"); // the subject/principal is whom the token is about
        claims.setClaim("email","mail@example.com"); // additional claims/attributes about the subject can be added

        // A JWT is a JWS and/or a JWE with JSON Claims as the payload.
        // In this example it is a JWS so we create a JsonWebSignature object.
        JsonWebSignature jws = new JsonWebSignature();

        // The payload of the JWS is JSON content of the JWT Claims
        jws.setPayload(claims.toJson());

        // Set the signature algorithm on the JWT/JWS that will integrity protect the claims
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.NONE);
        jws.setAlgorithmConstraints(AlgorithmConstraints.NO_CONSTRAINTS);

        // Sign the JWS and produce the compact serialization or the complete JWT/JWS
        // compact serialization, which is where the JWT is a JWS
        // The compact serialization is a string consisting of three dot ('.') separated
        // base64url-encoded parts in the form Header.Payload.Signature
        String jwt = jws.getCompactSerialization();

        // Do something with the JWT. Like send it to some other party over the clouds
        // and through the interwebs.
        System.out.println("JWT: " + jwt);

        // Use JwtConsumerBuilder to construct an appropriate JwtConsumer, which will
        // be used to validate and process the JWT. But here, it's just used to parse it.
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime() // the JWT must have an expiration time
                .setJwsAlgorithmConstraints(AlgorithmConstraints.NO_CONSTRAINTS)
                // disable signature requirement
                .setDisableRequireSignature()
                .build(); // create the JwtConsumer instance

        try {

        //    String token = "eyJhbGciOiJub25lIn0.eyJleHAiOjMwMCwianRpIjoiSDkyV1Nab0M1SUVQODY5blVxc0p0dyIsImlhdCI6MTY5MDQ4MjQzMSwidXNlcklkIjoiVVMuOTQ4NTE2NzUzMzU2NDgyNDYiLCJhdXRob3JpemF0aW9uUHJvZmlsZUNvZGUiOiJOZXRhZG1pbkRlZmF1bHQiLCJvcmlnaW5hbFNlcnZpY2VSZXF1ZXN0SWQiOiI2NTBhNmY0Zi1jYzNjLTQ4ZGYtOGJhZi0xZTRhMzBjMGZjNzAiLCJuYW1lIjoiU3lzdGVtIEFkbWluK09uZSIsImNhdGVnb3J5Q29kZSI6Ik5XQURNIiwiaWRlbnRpZmllclR5cGUiOiJMT0dJTklEIiwiaWRlbnRpZmllclZhbHVlIjoic3lzYWRtaW5vbmUiLCJiZWFyZXJDb2RlIjoiV0VCIiwic2VydmljZVJlcXVlc3RJZCI6IjY1MGE2ZjRmLWNjM2MtNDhkZi04YmFmLTFlNGEzMGMwZmM3MCIsInRlbmFudElkIjoibWZzUHJpbWFyeVRlbmFudCIsImNsaWVudF9pZCI6IkNvcmVXZWIiLCJzZXNzaW9uX2lkIjoiNjUwYTZmNGYtY2MzYy00OGRmLThiYWYtMWU0YTMwYzBmYzcwIiwic2NvcGUiOlsiR0VUX1VTRVJfQU5EX0FDQ09VTlRfREVUQUlMUyIsIlNFTEZTRVRBVVRIRkFDVE9SIiwiRkVUQ0hfVVNFUl9RVUVTVElPTiIsIlRYTkNPUlJFQ1QiLCJBVE1DQVNIT1VUIiwiQVRNQ0FTSE9VVF9WNCIsIkFETVRYTlJFRklEIl0sImF1dGhvcml0aWVzIjpbIlJPTEVfQ0xJRU5UIl19.";

            // Validate the JWT and process it to the Claims
            JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
            System.out.println("JWT validation succeeded! " + jwtClaims);
        } catch (Exception e) {
            System.out.println("JWT validation failed!" + e);
        }
    }
}
