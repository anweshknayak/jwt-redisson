package jwt;

import org.jose4j.jwe.*;
import org.jose4j.jwk.*;
import org.jose4j.lang.*;

public class SignedJWT {

    public static void main(String[] args) throws JoseException {


// Generate a key pair
        RsaJsonWebKey rsaJsonWebKey = RsaJwkGenerator.generateJwk(2048);
        rsaJsonWebKey.setKeyId("k1");

        System.out.println("rsaJsonWebKey" + rsaJsonWebKey.getKey().toString());

// Create a new Json Web Encryption object
        JsonWebEncryption jwe = new JsonWebEncryption();
        jwe.setPayload("This is the text to be encrypted");
        jwe.setAlgorithmHeaderValue(KeyManagementAlgorithmIdentifiers.RSA_OAEP);
        jwe.setEncryptionMethodHeaderParameter(ContentEncryptionAlgorithmIdentifiers.AES_128_CBC_HMAC_SHA_256);
        jwe.setKey(rsaJsonWebKey.getKey());

// Encrypt the text
        String encryptedJwt = jwe.getCompactSerialization();
        System.out.println("encryptedJwt :::: " + encryptedJwt);

// To decrypt
        JsonWebEncryption jwtConsumer = new JsonWebEncryption();
        jwtConsumer.setKey(rsaJsonWebKey.getRsaPublicKey());
        jwtConsumer.setCompactSerialization(encryptedJwt);

        System.out.println("AlgorithmHeaderValue :::: " + rsaJsonWebKey.getRsaPublicKey());

// Decrypt the text
        String decryptedJwt = jwtConsumer.getPayload();
        System.out.println("decryptedJwt :::: " + decryptedJwt);

    }
}
