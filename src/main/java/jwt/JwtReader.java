package jwt;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Map;

/**
 * Created by suresh.sahu1 on 24-12-2020.
 */
public class JwtReader {


    public static void main(String[] args) throws InvalidJwtException {

        String token = "eyJraWQiOiJrMSIsImFsZyI6IlJTMjU2In0.eyJleHAiOjE2OTA0ODEwNTcsImp0aSI6Im0xM0h3d1hHeHMzZGwyZGpGX3Y0blEiLCJpYXQiOjE2OTA0ODA0NTcsImVtYWlsIjoibWFpbEBleGFtcGxlLmNvbSJ9.Y6SUmfaGMm-8j7chplwgANXb5Md8GrkbcPK5sKw7zc0VusYbN2WfZrxldG4PvmkpfZa3R_Zbg0BXbXsPw5_TA8IdLXtws0EnoFYVPSlMQUnftpFC6iyg5PT253sPgPb9YsWtb5pLKdalvejBTP5t24HlHbJpzxpiaZnWeKMBi6Gvnbl3Jvhs92ZKJjYUc-_6f0ZT0peDZ39FzVs8VICXUb_cLg0jliSw6x1N8nwJlvCpvqx9DCROkLWeyZPNRRHgu0KgbNBji5ONEKzof-jh6gPDBwoBgwYLlaDfn7azcDlUYjMs0QujRwl_4kNLRj_bXH2WdNaotKEJKOjaMQ1xdg";
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireExpirationTime()
                //          .setVerificationKey(publicKey)
                .build();

        // validate and decode the jwt
        JwtClaims jwtDecoded = jwtConsumer.processToClaims(token);
        Map<String,Object> jwtClaims = jwtDecoded.getClaimsMap();
        System.out.println(jwtClaims);
    }



}
