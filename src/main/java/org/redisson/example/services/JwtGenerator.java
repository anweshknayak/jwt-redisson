package org.redisson.example.services;

import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.NumericDate;
import org.jose4j.lang.JoseException;
import org.jose4j.keys.HmacKey;
import org.jose4j.jws.AlgorithmIdentifiers;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.SecureRandom;
import java.util.Base64;

public class JwtGenerator {

    public static final String USERIDENTIFIER = "userIdentifier";

    public JwtGenerator() {
    }

    //generate secret key
    public static String generateSecretKey() {
        SecureRandom secureRandom = new SecureRandom(); //creates a secure random number generator
        byte[] key = new byte[32]; //creates a byte array with 32 bytes
        secureRandom.nextBytes(key); //generates random bytes and places them into a user-supplied byte array
        String SECRET_KEY = Base64.getEncoder().encodeToString(key);
        return SECRET_KEY;
    }

    //generate JWT
    public String generateJwtAndSetClaims(){
        try {
            // Create the Claims, which will be the content of the JWT
            JwtClaims claims = new JwtClaims();
            claims.setIssuer("Issuer");  // who creates the token and signs it
            NumericDate fiveSecsInTheFutureNumericDate = NumericDate.fromSeconds(1);
            claims.setExpirationTime(fiveSecsInTheFutureNumericDate); // time when the token will expire (10 minutes from now)
            claims.setGeneratedJwtId(); // a unique identifier for the token
            claims.setIssuedAtToNow();  // when the token was issued/created (now)
            claims.setSubject("subject"); // the subject/principal is whom the token is about
            claims.setClaim("username", "user"); // additional claims/attributes about the subject can be added

            // A JWT is a JWS and/or a JWE with JSON Claims as the payload.
            // In this example it is a JWS so we create a JsonWebSignature object.
            // Create JWS
            JsonWebSignature jws = new JsonWebSignature();
            jws.setPayload(claims.toJson());
            jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
            jws.setKey(new HmacKey(generateSecretKey().getBytes()));

            // Compact Serialization
            // (e.g. eyJhbGciOiJIUzI1NiJ9...)
            String jwt = jws.getCompactSerialization();




            return jwt;
        } catch (JoseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        try {
            // Create the Claims, which will be the content of the JWT
            JwtClaims claims = new JwtClaims();
            claims.setIssuer("Issuer");  // who creates the token and signs it
            claims.setExpirationTimeMinutesInTheFuture(10); // time when the token will expire (10 minutes from now)
            claims.setGeneratedJwtId(); // a unique identifier for the token
            claims.setIssuedAtToNow();  // when the token was issued/created (now)
            claims.setSubject("subject"); // the subject/principal is whom the token is about
            claims.setClaim("username", "user"); // additional claims/attributes about the subject can be added

            // A JWT is a JWS and/or a JWE with JSON Claims as the payload.
            // In this example it is a JWS so we create a JsonWebSignature object.
            // Create JWS
            JsonWebSignature jws = new JsonWebSignature();
            jws.setPayload(claims.toJson());
            jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
            jws.setKey(new HmacKey(generateSecretKey().getBytes()));

            // Compact Serialization
            // (e.g. eyJhbGciOiJIUzI1NiJ9...)
            String jwt = jws.getCompactSerialization();

            System.out.println("JWT: " + jwt);

        } catch (JoseException e) {
            e.printStackTrace();
        }
    }
}
