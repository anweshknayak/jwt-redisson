/**
 * Copyright (c) 2016-2019 Nikita Koksharov
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.redisson.example.services;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.NumericDate;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.redisson.api.map.event.*;
import org.redisson.config.Config;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static jwt.JwtGenerator.generateSecretKey;

public class TransactionExpiry {


    public static void main(String[] args) throws IOException, JoseException {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.25.41.59:6379");
        RedissonClient redisson = Redisson.create(config);

        JwtClaims claims = new JwtClaims();
        claims.setIssuer("Issuer");  // who creates the token and signs it
        // 5 seconds
        NumericDate oneSecondFromNow = NumericDate.fromMilliseconds(System.currentTimeMillis() + 1000);
        claims.setExpirationTime(oneSecondFromNow);
        claims.setGeneratedJwtId(); // a unique identifier for the token
        claims.setIssuedAtToNow();  // when the token was issued/created (now)
        claims.setSubject("subject"); // the subject/principal is whom the token is about
        claims.setClaim("username", "user"); // additional claims/attributes about the subject can be added

        // A JWT is a JWS and/or a JWE with JSON Claims as the payload.
        // In this example it is a JWS so we create a JsonWebSignature object.
        // Create JWS
        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
        jws.setKey(new HmacKey(generateSecretKey().getBytes()));
        String jwt =  jws.getCompactSerialization();

        RMapCache<String, Object> tokenMap = redisson.getMapCache("mykey");
        tokenMap.put(jwt, claims.getClaimsMap(), 1, TimeUnit.SECONDS);

        //wait for 2 seconds
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        // populate the token details and claims generated during login
        RMapCache<String, Object> map = redisson.getMapCache("jwtCache");
        map.put(jwt, claims.getClaimsMap(), 1, TimeUnit.SECONDS);

        // expiredEventsCache is used to store the expired events
        RMapCache<String, Object> expiredEventsCache = redisson.getMapCache("expiredEventsCache");

        // auditCache is used to store the audit all type of events like EntryCreated, EntryUpdated, EntryRemoved
        RMapCache<String, Object> auditCache = redisson.getMapCache("auditCache");

        int updateListener = map.addListener(new EntryUpdatedListener<Integer, Integer>() {
            @Override
            public void onUpdated(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                event.getOldValue();
                System.out.println("RMapCache  ::::  onUpdated ::::" + event.getKey() + "  " + event.getValue() + "  " + event.getOldValue());
                expiredEventsCache.put(String.valueOf(event.getKey()), event.getValue(), 10, TimeUnit.SECONDS);
                auditCache.put(String.valueOf(event.getKey()), event.getValue(), 10, TimeUnit.SECONDS);
            }
        });

        int createListener = map.addListener(new EntryCreatedListener<Integer, Integer>() {
            @Override
            public void onCreated(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                System.out.println("RMapCache  ::::  onCreated ::::" + event.getKey() + "  " + event.getValue());
                auditCache.put(String.valueOf(event.getKey()), event.getValue(), 10, TimeUnit.SECONDS);

            }
        });

        int expireListener = map.addListener(new EntryExpiredListener<Integer, Integer>() {
            @Override
            public void onExpired(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                System.out.println("RMapCache  ::::  onExpired ::::" + event.getKey() + "  " + event.getValue());
                auditCache.put(String.valueOf(event.getKey()), event.getValue(), 10, TimeUnit.SECONDS);
            }
        });

        int removeListener = map.addListener(new EntryRemovedListener<Integer, Integer>() {
            @Override
            public void onRemoved(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                System.out.println("RMapCache  ::::  onRemoved ::::" + event.getKey() + "  " + event.getValue());
                auditCache.put(String.valueOf(event.getKey()), event.getValue(), 10, TimeUnit.SECONDS);
            }
        });

        System.out.println("RMapCache  :::: " + map.readAllMap());

    }

}
