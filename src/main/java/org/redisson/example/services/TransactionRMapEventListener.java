/**
 * Copyright (c) 2016-2019 Nikita Koksharov
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.redisson.example.services;

import org.jose4j.lang.JoseException;
import org.redisson.Redisson;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.redisson.api.map.event.*;
import org.redisson.config.Config;

import java.io.IOException;

public class TransactionRMapEventListener {


    public static void main(String[] args) throws IOException, JoseException {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.25.41.59:6379");
        RedissonClient redisson = Redisson.create(config);


        RMapCache<String, Object> map = redisson.getMapCache("mykey");
        map.put("1", 1);
        System.out.println("RMapCache  :::: " + map.readAllMap());
        int updateListener = map.addListener(new EntryUpdatedListener<Integer, Integer>() {
            @Override
            public void onUpdated(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                event.getOldValue();
                System.out.println("RMapCache  ::::  onUpdated ::::" + event.getKey() + "  " + event.getValue() + "  " + event.getOldValue());
                // ...
            }
        });

        int createListener = map.addListener(new EntryCreatedListener<Integer, Integer>() {
            @Override
            public void onCreated(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                System.out.println("RMapCache  ::::  onCreated ::::" + event.getKey() + "  " + event.getValue());
            }
        });

        int expireListener = map.addListener(new EntryExpiredListener<Integer, Integer>() {
            @Override
            public void onExpired(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                System.out.println("RMapCache  ::::  onExpired ::::" + event.getKey() + "  " + event.getValue());
            }
        });

        int removeListener = map.addListener(new EntryRemovedListener<Integer, Integer>() {
            @Override
            public void onRemoved(EntryEvent<Integer, Integer> event) {
                event.getKey(); // key
                event.getValue();
                System.out.println("RMapCache  ::::  onRemoved ::::" + event.getKey() + "  " + event.getValue());
            }
        });

        map.put("1", 2, 1, java.util.concurrent.TimeUnit.SECONDS);
        map.put("2", 2, 1, java.util.concurrent.TimeUnit.SECONDS);
    }

}
