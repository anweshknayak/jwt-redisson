package org.redisson.example.services;

import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.UUID;

public class TransactionRMapAudit {


    public static void main(String[] args) {

        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.25.41.59:6379");
        RedissonClient redisson = Redisson.create(config);

// Create AuditLog entry
        AuditLog auditLog = new AuditLog();
        auditLog.setId(UUID.randomUUID().toString());
        auditLog.setToken("jwt");
        auditLog.setUsername("username");

// Store AuditLog entry in Redis
        RMap<String, String> auditLogs = redisson.getMap("auditLogs");
        auditLogs.put(auditLog.getId(), "1");
        System.out.println("auditLogs  :::: " + auditLogs.readAllMap());
        auditLogs.put(auditLog.getId(), "12");
        System.out.println("auditLogs  :::: " + auditLogs.readAllMap());
    }
}
