/**
 * Copyright (c) 2016-2019 Nikita Koksharov
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.redisson.example.services;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.NumericDate;
import org.jose4j.keys.HmacKey;
import org.jose4j.lang.JoseException;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.io.IOException;

import static org.redisson.example.services.JwtGenerator.generateSecretKey;

public class TransactionRMap {


    public static void main(String[] args) throws IOException, JoseException {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.25.41.59:6379");
        RedissonClient redisson = Redisson.create(config);

        RBucket<String> b = redisson.getBucket("test");
        b.set("123");
        System.out.println("test: " + b.get());

        JwtClaims claims = new JwtClaims();
        claims.setIssuer("Issuer");  // who creates the token and signs it
        // 5 seconds
        NumericDate fiveSecsInTheFutureNumericDate = NumericDate.fromSeconds(5);
        claims.setExpirationTime(fiveSecsInTheFutureNumericDate); // time when the token will expire (10 minutes from now)
        claims.setGeneratedJwtId(); // a unique identifier for the token
        claims.setIssuedAtToNow();  // when the token was issued/created (now)
        claims.setSubject("subject"); // the subject/principal is whom the token is about
        claims.setClaim("username", "user"); // additional claims/attributes about the subject can be added

        // A JWT is a JWS and/or a JWE with JSON Claims as the payload.
        // In this example it is a JWS so we create a JsonWebSignature object.
        // Create JWS
        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
        jws.setKey(new HmacKey(generateSecretKey().getBytes()));
        String jwt =  jws.getCompactSerialization();

        RMap<String, Object> tokenMap = redisson.getMap(jwt);
        tokenMap.putAll(claims.getClaimsMap());

        RMap<String, Object> readToken = redisson.getMap(jwt);
        System.out.println("readToken  :::: " + readToken.readAllMap());

    }

}
