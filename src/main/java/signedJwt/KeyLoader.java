package signedJwt;



import java.security.PrivateKey;
import java.security.PublicKey;
import java.io.StringReader;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;



public class KeyLoader {

    // These would be fetched from your application's configuration
    private static final String RSA_PRIVATE_KEY_STRING = "-----BEGIN PRIVATE KEY-----\n" +
            "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDhdbCzT9kDl5Gw36Ho8Tsqbvsb\n" +
            "hpdxIgwoix1vbpNM8noe0agl9ZKVs2dxmTDgQk9Z3a1cVutoloIcW8n4JE1z/GkGywpgXKTcE5KQ\n" +
            "uTLEWrafTOh/DjX2XgKYkZvoviUa+vWfSoYcG14GxwzaD96tlRNSlNGDfNKgLOEIF5gtC+/ebUXa\n" +
            "BtUFiRDrNk2tOAWOFAT4HfpVet9/WXHrKTAx9EjUJuNmdGhDxTYbBFD/+itHQPzw7+Z55UBgTcp6\n" +
            "iFJoEbyryb67miTxG2zWcW4FWHbL3a9f5ZD9lAbbKB1Ll9GhkMtdOk3UhEmoMFjCWrEdq1o9Hwdo\n" +
            "Rd/+C8LPVtabAgMBAAECggEAUjv7F2woW4cNPNZU2AIk8nVIYeHVmw4wIRmcwugyEf7fTB5slmvb\n" +
            "635kXKrjiOimyMByWxvZAWzkSDOyly+mkIegRzoHrt4bw6nf66mhOr1bnv8OTkmApTvk9oElvN6d\n" +
            "6hlfPZqRD+9rfE1eAquHLQkS4ywkvzDoIkQ8UBdLCiKBGM4Z9kgrpY6sMEyWOmu4kfadsPwuF4Se\n" +
            "9NORVxFtE4FmlaQUs7KEqL+YdChCqzE1HExmk3XzFMDazN9mye3CrPGDrtG7+/Uz42OTEB7ogbYB\n" +
            "WDyFBz/LF+7IAIU+HUysuAyv/WoTTwt7V3Ufyy9NmJdr4UjHTt82HQ3Y4zLeAQKBgQD5l1+Lw80z\n" +
            "7xep4bYTaSK0pmhXt+R2m3gjICzPcZyfSYYphphfmDQ57EKSPeDF20grj7hzpfL+XDwE9b2LKgPP\n" +
            "0ibcTWJRG+A4IB8VtgGlu97MfBFUO2l64MbfDlxSqw9dXzqauCFjxYXYxSP8xy4GxhFmfhPmsIZX\n" +
            "0cG0VOzi4wKBgQDnP7GuY0+KqjWU+HFr4gyLiDx5FBTa57uOLUirnSkjMJXwcswXQaookQn+z8QG\n" +
            "/nE5fUHokO/QWsqmiSBQsUhm4h/RSUUKx0u/UOfhXkYNKfZgelEDBvXkgk6yxiSRBosWpnoWfb/5\n" +
            "jc6xoGMoOsF577VSEWn2/02zZMsFYqQy6QKBgQD5iblSMGISIWJkS7FReNTlDlbWFJoLZ2bWn42N\n" +
            "nYY8zzKeg94F/VOB8c+hDNKUnqdqPhfpMCpg2vB6+QIXh9slxXNq4MeCVj93fc32U/ETtWCMyyI8\n" +
            "N+Y+n0rVqjLtx+7lqJccJyni6ZHnZzqSuMUwqoitEzJse2LlnGZxGFN6hwKBgQDMPOsXFEs6Mx5j\n" +
            "1hGGsP1XdxnB7kSFGkoCRmh6ebHNjKnA1gL/P1WjwoXLcYC2lJ83oTNGzkuf/7Af6diKrClABNMJ\n" +
            "Kuc5n898x14lI/hGxBsAFsfaHpmqGpSbPHkrRVYuyRXnK2UUaXR2WSt6anpa0aWZOBwYtF/R42GT\n" +
            "/gz7kQKBgFXz2rqD3f6xrcIzMKAeGvGb0qxn7+zo07OXyWOwgIBeEsErXPAUVIp5iE5xEB2KZwVZ\n" +
            "QsajbvlIHjyOx/xlf1c2nS7vrVpFbLfR/VVh3pIxrtYrqVGHcQsxB7JcpeukD5HkJmBUPn0fFbam\n" +
            "2kH7GO0/+62SNYG/3/8rtkluv7VE\n" +
            "-----END PRIVATE KEY-----"; // Load from your config
    private static final String RSA_PUBLIC_KEY_STRING = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4XWws0/ZA5eRsN+h6PE7Km77G4aXcSIM\n" +
            "KIsdb26TTPJ6HtGoJfWSlbNncZkw4EJPWd2tXFbraJaCHFvJ+CRNc/xpBssKYFyk3BOSkLkyxFq2\n" +
            "n0zofw419l4CmJGb6L4lGvr1n0qGHBteBscM2g/erZUTUpTRg3zSoCzhCBeYLQvv3m1F2gbVBYkQ\n" +
            "6zZNrTgFjhQE+B36VXrff1lx6ykwMfRI1CbjZnRoQ8U2GwRQ//orR0D88O/meeVAYE3KeohSaBG8\n" +
            "q8m+u5ok8Rts1nFuBVh2y92vX+WQ/ZQG2ygdS5fRoZDLXTpN1IRJqDBYwlqxHataPR8HaEXf/gvC\n" +
            "z1bWmwIDAQAB\n" +
            "-----END PUBLIC KEY-----"; // Load from your config

    public static PrivateKey loadPrivateKey(String privateKeyPEM) throws Exception {
        PemReader pemReader = new PemReader(new StringReader(privateKeyPEM));
        PemObject pemObject = pemReader.readPemObject();
        byte[] keyBytes = pemObject.getContent();
        pemReader.close();

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    public static PublicKey loadPublicKey(String publicKeyPEM) throws Exception {
        PemReader pemReader = new PemReader(new StringReader(publicKeyPEM));
        PemObject pemObject = pemReader.readPemObject();
        byte[] keyBytes = pemObject.getContent();
        pemReader.close();

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    public static void main(String[] args) throws Exception {

        PrivateKey privateKey = loadPrivateKey(RSA_PRIVATE_KEY_STRING);

        PublicKey publicKey = loadPublicKey(RSA_PUBLIC_KEY_STRING);
        // You can now use these keys to sign and verify JWTs as shown in the previous example

        // Create JWT claims
        JwtClaims claims = new JwtClaims();
        claims.setIssuer("your_issuer");
        claims.setSubject("your_subject");
        claims.setExpirationTimeMinutesInTheFuture(10); // expires in 10 minutes

        // Sign the JWT
        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
        jws.setKey(privateKey);
        String jwt = jws.getCompactSerialization();

        System.out.println("Generated JWT: " + jwt);

        // Verify JWT
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireSubject()
            //    .setExpectedIssuer("your_issuer")
            //    .setExpectedAudience("your_audience")
                .setVerificationKey(publicKey)
                .build();

        try {
            JwtClaims verifiedClaims = jwtConsumer.processToClaims(jwt);
            System.out.println("JWT verified! Subject: " + verifiedClaims.getSubject());
        } catch (Exception e) {
            System.out.println("JWT verification failed: " + e.getMessage());
        }
    }
}
