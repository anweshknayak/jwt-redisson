package signedJwt;


import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.RsaKeyUtil;

import java.io.StringReader;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;


public class KeyLoaderV2 {

    // These would be fetched from your application's configuration
    private static final String RSA_PRIVATE_KEY_STRING = "-----BEGIN PRIVATE KEY----- MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCzOlZiaJMa/tjiWXi5s9Ne5O+n MkDB+aCTuVEx7kkNxc1UmuNOV7QUBXuMCUHnjR4oVNI6DCTWf5+acgLGvn2gT7q+j83tvWD/DtU5 5NlnjtpcY1y4y42Zn/ytAWXgEZpZPzs3ldArecHfSJf8o2hWJajyJbw/tnX7ObxFPqPf9slo6U8u uag8NHQi1wh3cOczfG0GbHoiUhdWyUjwJryV4D+WTPop0P1S6fIw+o/E2N/sAOY54MaeXBXxnE+K D6Cmxp8rRk0/w6lT6fL2aiwVkBNu/oDM6meF0PK/fyQK4Brh4LOrSq8vcb+ranr67SqncEu56PdL mcEenkLWfWGJAgMBAAECggEATmW3QaaSql1Ni30tlLV3QOwNdGpD71V9CwEl5hxgMjkSvu5dfXhZ meRoDXJDNMECrwujRaboOjdrHgi+9CaBFy2o9xA6OML8+bSISqGzA1x2YbFI7sCDOO7FfPu+j0vD 40QIaCENBOdmiX/ck7L6wawJgZpGBOyQIQs5MmTbE+zHD1lUa7S0v/cmCbMRtcamcrxWH3vTiXNq t3XVuqmx+JOhWmp28tYAx43X17nDbNy1bCPCkh10UZE5LpexRcS/IlBk/oTAP75PrBHz5vxN4vkw VFrzN0d1mhxpsQAgDOhtEOptYFcNcwCAOd9a6i1gKIfGjC46L18Qwauur4GUrQKBgQD3HsoOucPD qhUFXC2JSuX1IBV0FmJhMSj17ZxuQC7fkL5g049CMIf70q3A7xbaFYc5pUkvgY7N3r8VKz5+iuvO 21rmtwy01AqP8HLepFbmB52hvLon1hdo+7ZATiMh/BJtr4dKczoJpECOsUYOaw54+4nwtrabq/4M niLbY8ovqwKBgQC5qwT48/j8z2JmVFi4WJeEaQ2S++Yr3MzmDanLQraWXxI5prF0Trhb7CQoa5+d uoizgxVrkK3b2/0H6617t/5QV8nr9G/mzhejhDBBOMrQ04Hy1snpDLuSVkF9xOuWEMOIO1ivYEzk JLgoxFnWSjrr4P+796AR+KqJcXLfZVaPmwKBgFGxMvi1PlsTgcl41HDberOS/KSYtcMNfk32U806 mIQ9lEHsDiLCamG8HsZ2A2bq0P4kftIekTfLV/3Ggu1QPW3vo9g7UQ8CPc+hnEevqi52/Aw4mI2M v8Zr167gZcrZSrpJ2X6qItiHHPRO9CrWBz3FYa9ap3lE3UIMkNdX4MHrAoGAVb9I9ZzfYgp5HF9Z 9hFcMJe/4CEcBSfdLWYkA66U2s+fgCafD+MHO9TMQiqqNdMfLSCd9OaFlON9cXoDzf6EoiBZLfLe 9045FpiHHKgY6uazvi46Y7owqn3Rkz8tukmVtEYMSDqGaFsY/iLohpITsjpggTP0pjgcsotFjSyl FH8CgYBh4nsyF4hJzEpFkukOONwa+n2HXLkbXlIARbLB0cpXoEfpD6VO25Rdxy8NmT4JCNE3x/Ct RdJT3Y4xaCw2YhYiFhXJdrpRmjaHRjkDvbPPgWTMPLWQyI6d85RSCPMCF8i25233ajqukUkrCQLV MqJ2EScXNn7FM51WlQ73Qldbyg== -----END PRIVATE KEY-----";


    private static final String RSA_PUBLIC_KEY_STRING = "-----BEGIN PUBLIC KEY----- MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAszpWYmiTGv7Y4ll4ubPTXuTvpzJAwfmg k7lRMe5JDcXNVJrjTle0FAV7jAlB540eKFTSOgwk1n+fmnICxr59oE+6vo/N7b1g/w7VOeTZZ47a XGNcuMuNmZ/8rQFl4BGaWT87N5XQK3nB30iX/KNoViWo8iW8P7Z1+zm8RT6j3/bJaOlPLrmoPDR0 ItcId3DnM3xtBmx6IlIXVslI8Ca8leA/lkz6KdD9UunyMPqPxNjf7ADmOeDGnlwV8ZxPig+gpsaf K0ZNP8OpU+ny9mosFZATbv6AzOpnhdDyv38kCuAa4eCzq0qvL3G/q2p6+u0qp3BLuej3S5nBHp5C 1n1hiQIDAQAB -----END PUBLIC KEY-----";

/*    public static PrivateKey loadPrivateKey(String privateKeyPEM) throws Exception {
        System.out.println(privateKeyPEM);
        PemReader pemReader = new PemReader(new StringReader(privateKeyPEM));
        PemObject pemObject = pemReader.readPemObject();
        byte[] keyBytes = pemObject.getContent();
        pemReader.close();

        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }*/

    public static PublicKey loadPublicKey(String publicKeyPEM) throws Exception {
        System.out.println(publicKeyPEM);
        PemReader pemReader = new PemReader(new StringReader(publicKeyPEM));
        PemObject pemObject = pemReader.readPemObject();
        byte[] keyBytes = pemObject.getContent();
        pemReader.close();

        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    public static PrivateKey loadPrivateKey(String privateKeyPEM) throws Exception {
        String privateKeyPEMContents = privateKeyPEM
                .replace("-----BEGIN PRIVATE KEY-----", "")
                .replace("-----END PRIVATE KEY-----", "")
                .replaceAll("\\s", ""); // remove whitespaces

        byte[] privateKeyDER = Base64.getDecoder().decode(privateKeyPEMContents);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(privateKeyDER);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    public static void main(String[] args) throws Exception {
        RsaKeyUtil rsaKeyUtil = new RsaKeyUtil();

        PrivateKey privateKey = loadPrivateKey(RSA_PRIVATE_KEY_STRING);
        PublicKey publicKey = rsaKeyUtil.fromPemEncoded(RSA_PUBLIC_KEY_STRING);
        // You can now use these keys to sign and verify JWTs as shown in the previous example

        // Create JWT claims
        JwtClaims claims = new JwtClaims();
        claims.setIssuer("your_issuer");
        claims.setSubject("your_subject");
        claims.setExpirationTimeMinutesInTheFuture(10); // expires in 10 minutes

        // Sign the JWT
        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
        jws.setKey(privateKey);
        String jwt = jws.getCompactSerialization();

        System.out.println("Generated JWT: " + jwt);

        // Verify JWT
        JwtConsumer jwtConsumer = new JwtConsumerBuilder()
                .setRequireSubject()
            //    .setExpectedIssuer("your_issuer")
            //    .setExpectedAudience("your_audience")
                .setVerificationKey(publicKey)
                .build();

        try {
            JwtClaims verifiedClaims = jwtConsumer.processToClaims(jwt);
            System.out.println("JWT verified! Subject: " + verifiedClaims.getSubject());
        } catch (Exception e) {
            System.out.println("JWT verification failed: " + e.getMessage());
        }
    }
}
